//
//  Classmate.swift
//  TableView
//
//  Created by Jaxon Stevens on 7/5/17.
//  Copyright © 2017 Jaxon Stevens. All rights reserved.
//

import Foundation

class Classmate {
    
    let name: String!
    let country: String!
    
    
    init(name: String, country: String)  {
        
        self.name = name
        self.country = country
    }
    
}
