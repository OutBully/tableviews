//
//  Category.swift
//  TableView
//
//  Created by Jaxon Stevens on 7/5/17.
//  Copyright © 2017 Jaxon Stevens. All rights reserved.
//

import Foundation

class Category {
  
    let name: String!
    
    init(name: String) {
        self.name = name
    }
    
}
