//
//  TableViewCell2.swift
//  TableView
//
//  Created by Jaxon Stevens on 7/5/17.
//  Copyright © 2017 Jaxon Stevens. All rights reserved.
//

import UIKit

class TableViewCell2: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var MyLabel: UILabel!
    
    @IBOutlet weak var ClassmateLabel: UILabel!

    func setupCell(withClassmate classmate: Classmate){
        
        ClassmateLabel.text = classmate.name
        ClassmateLabel.text = classmate.country
    

    }
}
